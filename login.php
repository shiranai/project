    <?php
    require_once 'core/init.php';

    if(Session::exists('user')){
      header('Location: index.php');
    }

    //memanggil objek
    $validation = new Validation();

    if( Input::get('login') ){
      $validation = $validation->check(array(
        'username' => array( 'required' => true ),
        'password' => array( 'required' => true )
      ));

      if($validation->passed()){
        if ($user->login_cek(Input::get('username'))) {

        if ( $user->login_user(Input::get('username'),Input::get('password')) ) {
          Session::set('user',Input::get('username'));
          header('Location: index.php');
        }
        else{
          $errors[] = 'login gagal';
        }
      }else{
        $errors[] = 'username belum terdaftar';
      }

    } else{
      $errors[] = 'silahkan mengisi kolom yang tersedia';
    }

    }

    if ( Input::get('register') ) {
      //cek Validation

      $validation = $validation->check(array(
        'username' => array(
                             'required' => true,
                             'min'      => 6,
                             'max'      => 50,
                           ),
        'password' => array(
                             'required' => true,
                             'min'      => 6,
                           ),

         'nama' => array(
                              'required' => true,
                              'min'      => 3,
                            ),

      ));

      if ( $validation->passed() ){
        if (Input::get('password') == Input::get('password2')) {
          $user->register_user(array(
            'username' => Input::get('username'),
            'password' => password_hash(Input::get('password'), PASSWORD_DEFAULT),
            'email'    => Input::get('email'),
            'nama'     => Input::get('nama')
          ));

        }
        else {
          echo "<script>alert('Password yang anda masukkan tidak match !')</script>";
        }
      }
      else{
        $errors = $validation->errors();
      }

    }

    ?>
    <!DOCTYPE html>
    <html id=html>
      <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="assets/css/login.css">
      </head>
      <body>
        <script src="assets/javascript/login.js"></script>
    <div id="sidebar">
      <?php if(!empty($errors)){ ?> <script type="text/javascript">
        show_side('html','side')
      </script> <?php } ?>
      <button type="button" name="button" class=siden onclick=hide_side('html','side')> < Close</button>
      <div id="warning">
        <p>
          ERROR
        </p>
      </div>
      <?php if (!empty($errors)){ ?>
        <div id="errors">
          <ol id="error-ol">

          <?php foreach ($errors as $error){ ?>
            <li> <?php echo $error ?> </li>
            <?php } ?>
          </ol>
        </div>
          <?php } ?>
        </div>
    </div>
    <header>
      <!-- <button type="button" name="button" id=side onclick=show_side('html','side')> > </button> -->
      <ul class=menu>
        <li onclick=help('login-box','help-box')><a href="#">Help</a></li>
        <li onclick=change_form('login-box','text-box','submit','help-box') onmouseover="change_form('login-box','text-box','submit','help-box')"><a href="#">Login</a></li>
        <li onclick=change_form_reg('login-box','text-box','submit','help-box') onmouseover="change_form_reg('login-box','text-box','submit','help-box')"><a href="#">Register</a></li>
      </ul>
    </header>
    <div id="login-box">
      <form class="login-form" action="" method="post">
        <div id="text-box">
          <input type="text" name="username" placeholder="username" class="text">
          <input type="password" name="password" placeholder="password" class="text">
          <input type="password" name="password2" placeholder="Re-Type Password" class="text">
          <input type="email" name="email" placeholder="E-Mail" class="text">
          <input type="text" name="nama" placeholder="nama" class="text">
        </div>
        <input type="submit" name="login" value="register" class="submit" id="submit">
      </form>
    </div>
    <div id="help-box">
      <div class="help-title">
        <p>Help</p>
      </div>
      <div class="help-body">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
      <div class="close"  onclick=closz('help-box')>
        <a href="">close</a>
      </div>
    </div>
    <script src="assets/javascript/login.js"></script>


    <?php

    require_once 'templates/footer.php';

     ?>
